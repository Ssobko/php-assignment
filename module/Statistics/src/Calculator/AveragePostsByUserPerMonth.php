<?php

namespace Statistics\Calculator;

use SocialPost\Dto\SocialPostTo;
use Statistics\Dto\StatisticsTo;

/**
 * Class TotalPosts
 *
 * @package Statistics\Calculator
 */
class AveragePostsByUserPerMonth extends AbstractCalculator
{

    protected const UNITS = 'posts';

    /**
     * @var array
     */
    public $user_ids = [];

    /**
     * @var array
     */
    public $totals = [];

    /**
     * @param SocialPostTo $postTo
     */
    protected function doAccumulate(SocialPostTo $postTo): void
    {
        $key = $postTo->getDate()->format('\M\o\n\t\h M, Y');
        $this->totals[$key] = ($this->totals[$key] ?? 0) + 1;

        // Collect all unique users with posts per month
        $user = $postTo->getAuthorId();
        if(!array_key_exists($user, $this->user_ids))
            $this->user_ids[$key] = $user;
    }

    /**
     * @return StatisticsTo
     */
    public function doCalculate(): StatisticsTo
    {
        $stats = new StatisticsTo();
        foreach ($this->totals as $splitPeriod => $total) {
            $value = $total / count($this->user_ids[$splitPeriod]) > 0 ? count($this->user_ids[$splitPeriod]) : 1;
            $child = (new StatisticsTo())
                ->setName($this->parameters->getStatName())
                ->setSplitPeriod($splitPeriod)
                ->setValue($value)
                ->setUnits(self::UNITS);

            $stats->addChild($child);
        }

        return $stats;
    }
}
