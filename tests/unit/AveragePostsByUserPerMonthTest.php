<?

namespace Tests\unit;

use PHPUnit\Framework\TestCase;

/**
 * Class AveragePostsByUserPerMonthTest
 *
 * @package Tests\unit
 */
class AveragePostsByUserPerMonthTest extends TestCase
{
    /**
     * @var array
     */
    private $user_ids;
    
    /**
     * @var int
     */
    private $total;

    protected function setUp(): void {
        $this->user_ids = [];
        $this->total = 1;
    }
    
    /**
     * @testCalculationValuePerSplitPeriod
     */
    public function testCalculationValuePerSplitPeriod(): void
    {
        $calc = new AveragePostsByUserPerMonth();
        $calc->user_ids = $this->user_ids;
        $this->assertSame(1, $this->total / count($calc->user_ids) > 0 ? count($calc->user_ids) : 1);
    }
}
?>